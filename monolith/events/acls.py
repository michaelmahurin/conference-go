import json
import requests
from events.keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state, "per_page": 1}
    response = requests.get(url, params=params, headers=headers)

    image_dict = json.loads(response.content)

    try:
        return {"picture_url": image_dict["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

    # picture_url = image_dict["photos"][0]["src"]["original"]

    # return picture_url


# try:
# {"picture_url": image_dict["photos"[0]["src"]["original"]]}


def get_weather_data(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None
